<?php
/**
 * Created by PhpStorm.
 * User: rick
 * Date: 2016-01-27
 * Time: 9:16 AM
 */

class ArtworkCSVExport extends DataExtension {

    public function getExportFields() {
        return array(
            'Artist.Title' => 'Artist name',
            'Title' => 'Title',
            'Year' => 'Year',
            'Medium' => 'Medium',
            'MPTitleID' => 'Inventory ID',
            'Images.First.Filename' => 'Filename',
            'Height' => 'Height',
            'Width' => 'Width',
            'Depth' => 'Depth',
            //'' => 'Diameter',
            'Price' => 'Price',
            //'' => 'Image rights',
            //'' => 'Additional information',
            //'' => 'Signature',
            //'' => 'Provenance',
            //'' => 'Exhibition history',
            //'' => 'Literature',
            //'' => 'Series',
            //'' => 'Confidential notes'
        );
    }

}


class ExportArtworkForArtsy extends  BuildTask
{
    protected $title = 'Export for Artsy';

    protected $description = 'Create Zip file for Artsy';

    protected $enabled = true;

    function run($request) {
        $artworks = Artwork::get()->filter(array('Quantity:GreaterThan' => 0));

        if($artworks->count()) {



            // create a file pointer connected to the output stream
            $output = fopen('artsy.csv', 'w');


            $zip = new ZipArchive();
            if ($res = $zip->open('../assets/Artsy.zip', true) !== true) {
                switch($res) {
                    case ZipArchive::ER_EXISTS:
                        $ErrMsg = "File already exists.";
                        break;

                    case ZipArchive::ER_INCONS:
                        $ErrMsg = "Zip archive inconsistent.";
                        break;

                    case ZipArchive::ER_MEMORY:
                        $ErrMsg = "Malloc failure.";
                        break;

                    case ZipArchive::ER_NOENT:
                        $ErrMsg = "No such file.";
                        break;

                    case ZipArchive::ER_NOZIP:
                        $ErrMsg = "Not a zip archive.";
                        break;

                    case ZipArchive::ER_OPEN:
                        $ErrMsg = "Can't open file.";
                        break;

                    case ZipArchive::ER_READ:
                        $ErrMsg = "Read error.";
                        break;

                    case ZipArchive::ER_SEEK:
                        $ErrMsg = "Seek error.";
                        break;

                    default:
                        $ErrMsg = "Unknow (Code $rOpen)";
                        break;
                }
                echo $ErrMsg;
                return false;
            }

            //$res = $zip->open(BASE_PATH.'/test.zip');



            // output the column headings
            fputcsv($output, array(
                'Artist Name',
                'Title',
                'Year',
                'Medium',
                'Inventory ID',
                'Filename',
                'Height',
                'Width',
                'Depth',
                'Price'
                ));



            foreach($artworks as $artwork) {

                fputcsv($output, array(
                    $artwork->Artist()->Title,
                    $artwork->Title,
                    $artwork->Year,
                    $artwork->Medium,
                    $artwork->MPTitleID,
                    $artwork->Artist()->URLSegment.'-'.$artwork->Images()->First()->Name,
                    $artwork->Height,
                    $artwork->Width,
                    $artwork->Depth,
                    $artwork->Price
                ));

                echo BASE_PATH.'/'.$artwork->Images()->First()->Filename.'<br>';

                $zip->addFile(
                    BASE_PATH.'/'.$artwork->Images()->First()->Filename,
                    $artwork->Artist()->URLSegment.'-'.$artwork->Images()->First()->Name
                );
            }

            fclose($output);

            //Debug::show($output);

            $zip->addFile(BASE_PATH.'/framework/artsy.csv', 'artsy.csv');

            $zip->close();

            echo 'done';


        }
    }

}


class ExportArtworkForInvauable extends  BuildTask
{
    protected $title = 'Export for Invaluable';

    protected $description = 'Create Zip file for Invaluable';

    protected $enabled = true;

    function run($request) {
        $artworks = Artwork::get()->filter(array('Quantity:GreaterThan' => 0));

        if($artworks->count()) {



            // create a file pointer connected to the output stream
            $output = fopen('Invaluable.csv', 'w');


//            $zip = new ZipArchive();
//            if ($res = $zip->open('../assets/Invaluable.zip', true) !== true) {
//                switch($res) {
//                    case ZipArchive::ER_EXISTS:
//                        $ErrMsg = "File already exists.";
//                        break;
//
//                    case ZipArchive::ER_INCONS:
//                        $ErrMsg = "Zip archive inconsistent.";
//                        break;
//
//                    case ZipArchive::ER_MEMORY:
//                        $ErrMsg = "Malloc failure.";
//                        break;
//
//                    case ZipArchive::ER_NOENT:
//                        $ErrMsg = "No such file.";
//                        break;
//
//                    case ZipArchive::ER_NOZIP:
//                        $ErrMsg = "Not a zip archive.";
//                        break;
//
//                    case ZipArchive::ER_OPEN:
//                        $ErrMsg = "Can't open file.";
//                        break;
//
//                    case ZipArchive::ER_READ:
//                        $ErrMsg = "Read error.";
//                        break;
//
//                    case ZipArchive::ER_SEEK:
//                        $ErrMsg = "Seek error.";
//                        break;
//
//                    default:
//                        $ErrMsg = "Unknow (Code $rOpen)";
//                        break;
//                }
//                echo $ErrMsg;
//                return false;
//            }

            //$res = $zip->open(BASE_PATH.'/test.zip');



            // output the column headings
            fputcsv($output, array(
                'Item Number',
                'Title',
                'Description',
                'Price',
                'Dimensions',
                'Artist Name',
                'Medium',
                'Circa'
            ));

            $i = 1;

            foreach($artworks as $artwork) {

                fputcsv($output, array(
                    $i,
                    $artwork->Title,
                    $artwork->obj('Content')->NoHTML(),
                    $artwork->Price,
                    $artwork->Height. ' x '. $artwork->Width . (($artwork->Depth != 0.00) ? ' x '.$artwork->Depth : ''),
                    $artwork->Artist()->Title,
                    $artwork->Medium,
                    $artwork->Year
                ));

                $j = 0;
                if ($artwork->Images()->count()) {
                    foreach($artwork->Images() as $image) {
                        set_time_limit(300);

                        if($image->exists() && $image->appCategory() == 'image') {
                            echo BASE_PATH.'/'.$image->Filename.'<br>';
                            error_log ($image->Filename);
                            $resized = $image->SetRatioSize(750,1125);
                            if ($resized) {
                                if ($resized->getAbsoluteSize() > 307200) {
                                    echo BASE_PATH.'/'.$resized->Filename . ' = ' . $resized->getSize().'<br>';
                                }

                                copy(BASE_PATH.'/'.$resized->Filename, BASE_PATH.'/assets/invaluable/'.$i . '-' . $j . '.jpg');

//                                $zip->addFile(
//                                    $resized->Filename,
//                                    $i . '-' . $j . '.jpg'
//                                );
                                $j++;
                            }

                        }


                    }
                }
                $i++;
            }

            fclose($output);
            copy(BASE_PATH.'/framework/Invaluable.csv', BASE_PATH.'/assets/invaluable/Invaluable.csv');
            //Debug::show($output);

//            $zip->addFile(BASE_PATH.'/framework/Invaluable.csv', 'Invaluable.csv');
//
//            $zip->close();

            echo 'done';


        }
    }

}

