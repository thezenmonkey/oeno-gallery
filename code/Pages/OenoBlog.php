<?php
	
class ExternalItem extends BlogPost {
	
}

class ExternalItem_Controller extends BlogPost_Controller {
	
}


class NewsItem extends BlogPost {
	
}

class NewsItem_Controller extends BlogPost_Controller {
	
}

class OenoBlog extends DataExtension {
	
	private static $db = array(
		"ExternalLink" => "Varchar(255)"
	);
	
	private static $has_one = array(
		
	);
	
	private static $has_many = array(
		
	);
	
	public function updateCMSFields(FieldList $fields) {
		if($this->owner->ClassName == "ExternalItem") {
			$fields->insertAfter(TextField::create("ExternalLink"), "FeaturedImage");
		}
	}
}