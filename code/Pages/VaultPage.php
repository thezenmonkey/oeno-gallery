<?php

class VaultPage extends Page {

	/**
	 * Template accessors
	 * ----------------------------------*/

	public function Artwork() {

		if( $member = Member::currentUser() ) {
			// Yes!

			$artwork = $member->VaultArtWorks()->filter(array(
				"ShowOnWeb" => 1,
				"Quantity:GreaterThan" => 0
			))->sort(array("Created" => "DESC"));

			if($artwork->count()) {
				return $artwork;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public function AllVault() {

		$artwork = VaultArtwork::get();

		if ($artwork->count()) {
			return $artwork;
		} else {
			return false;
		}
	}

}

class VaultPage_Controller extends Page_Controller {
	private static $allowed_actions = array (

	);

	public function init() {
		parent::init();

	}
}
