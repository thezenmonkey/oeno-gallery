<?php

class ArtworkCategoryPage extends ArtworkHolder {

    /**
     * Static vars
     * ----------------------------------*/



    /**
     * Object vars
     * ----------------------------------*/



    /**
     * Static methods
     * ----------------------------------*/



    /**
     * Data model
     * ----------------------------------*/

    private static $db = array ();


    private static $has_one = array (
        "DisplayCategory" => "ArtworkCategory"
    );

    private static $has_many = array (

    );

    /**
     * Common methods
     * ----------------------------------*/

    public function getCMSFields()
    {
        $fields = parent::getCMSFIelds();

        $fields->insertBefore(DropdownField::create("DisplayCategoryID", "Category", ArtworkCategory::get()->map('ID', 'Title')), "Content");

        return $fields;
    }

    /**
     * Accessor methods
     * ----------------------------------*/



    /**
     * Controller actions
     * ----------------------------------*/



    /**
     * Template accessors
     * ----------------------------------*/



    /**
     * Object methods
     * ----------------------------------*/




}


class ArtworkCategoryPage_Controller extends ArtworkHolder_Controller {

    private static $allowed_actions = array (
    );

    public function init() {
        parent::init();

    }

    public function RepresentedArtists() {

        return  false;
    }


    public function OtherArtists() {


        $artistList = new ArrayList();
        $artists = Artist::get();

        foreach ($artists as $artist) {
            if($artist->Artworks()->filter(
                    array("Quantity:GreaterThan" => 0, "ArtworkCategory" => $this->DisplayCategoryID)
                )->count() != 0) {
                $artistList->push($artist);
            }
        }

        return new PaginatedList($artistList->sort("Contact.LastName", "ASC"), $this->request);
    }


    public function PaginatedArtwork() {

        $displayCat = $this->DisplayCategoryID;



        $artworkQuery = new SQLQuery();
        $artworkQuery->setFrom('Artwork');
        $artworkQuery->setSelect('ID', 'ArtistID');
        $artworkQuery->addWhere("ClassName = 'Artwork'");

        if($this->DisplayCategory()->Title == "Sculpture") {
            $outdoorID = ArtworkCategory::get()->filter(array("Title" => "Outdoor Sculpture"))->First()->ID;
            $glassID = ArtworkCategory::get()->filter(array("Title" => "Glass"))->First()->ID;
            $artworkQuery->addWhere("Quantity > 0 AND (CategoryID = $displayCat OR CategoryID = $outdoorID OR CategoryID = $glassID)");
        } else {
            $artworkQuery->addWhere("Quantity > 0 AND CategoryID = $displayCat");
        }

        $artworkQuery->addGroupBy('ArtistID');
        $artworkQuery->addOrderBy('MPDateAdded', 'DESC');

        $result = $artworkQuery->execute();



        foreach($result as $row) {
            $artworkIDs[] = $row['ID'];
        }

        //$artwork = Artwork::get()->filter(array("Quantity:GreaterThan" => 0, "CategoryID" => $this->DisplayCategoryID))->sort(array("MPDateAdded" => "DESC"));
        //$artwork = Artwork::get()->byIDs($artworkIDs)->leftJoin("Artist", "\"Artist\".\"ID\" = \"Artwork\".\"ArtistID\""); //->sort(array('ArtistID.LastName' => 'ASC'));
        $artwork = Artwork::get()->byIDs($artworkIDs);

        $sortList = new ArrayList();

        foreach ($artwork as $item) {
            $item->LastName = $item->getLastName();
            $sortList->push($item);
        }

        if($artwork->count()) {
            return new PaginatedList($sortList->sort("LastName"), $this->request);
        } else {
            return false;
        }
    }
}
