<?php

class AjaxFormPage extends SiteTree {

    private static $db = array(
    );

    private static $has_one = array(
    );

}
class AjaxFormPage_Controller extends ContentController {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array (
        'index',
        'PrevNextPage',
        'MPReSync',
        'MPReSyncImages',
        'Purchase',
        'view',
        'art',
        'doEmail',
        'EmailThis',
        'SyncArtistPicture',
        'HoldForm',
        'doHold',
        'InfoForm',
        'renderFrom'
    );

    private static $url_handlers = array(
        'renderFrom//$Action/$ID/$Name' => 'Form'
    );


    public function Form() {
        $formRequest = $this->request->param('Action');
        $formRequestID = $this->request->param('ID');
        Debug::show($this->request);
        if($this->request->param('Action')) {
            Debug::show('woot');
            return $this->customise(new ArrayData(array(
                'Stuff' => 'woot',
                'Role' => 'Head Coach',
                'Experience' => $experience
            )))->renderWith("AjaxPage");
        } else {
            Debug::show('poot');
            return false;
        }




    }


    public function Purchase() {

        $actionArray = array('doPurchase', 'doHold', 'doInfo');

        if( !in_array($this->request->param('Action'), $actionArray) ) {
            $artwork = Artwork::get()->filter(array("URLSegment" => $this->request->param('Action'), "ArtistID" => $this->ID))->first();
        } else {
            $artwork = false;
        }

        if(!$artwork) {
            $pageID = $this->ID;
        } else {
            $pageID = $artwork->ID;
        }

        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                ->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email")
                ->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number")
                ->setAttribute('required', true),
            TextField::create('Address', false)
                ->setAttribute('placeholder', 'Address')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Address"),
            TextField::create('Address2', false)
                ->setAttribute('placeholder', 'Address Line 2 (Optional)')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Address Line 2 (Optional)"),
            TextField::create('City', false)
                ->setAttribute('placeholder', 'City')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("City"),
            TextField::create('Province', false)
                ->setAttribute('placeholder', 'State or Province')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("State or Province"),
            TextField::create('PostalCode', false)
                ->setAttribute('placeholder', 'Zip/Postal Code')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Zip/Postal Code"),
            TextField::create('Country', false)
                ->setAttribute('placeholder', 'Country')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Country"),
            CheckboxField::create('Confirm', 'I agree to purchase this work')
                ->setAttribute('required', null)
                ->addExtraClass(' left-field half-field'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Name', 'Email', 'Phone'
        ));

        $form = Form::create(
            $this,
            "Purchase",
            $fields,
            new FieldList(
                FormAction::create('doPurchase', 'Submit')->addExtraClass("button primary")->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array("name" => "PurchaseCaptcha"));
    }

    public function doPurchase($data, Form $form) {

        $artwork = Artwork::get()->byID($data['ArtworkID']);

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        $clientEmail = new Email();
        $galleryEmail = new Email();

        $clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');


        $clientEmail->setFrom($clientFrom)
            ->setTo($data['Email'])
            ->setSubject('Purchase info for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('CustomerEmailPurchase')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork,
                'SiteConfig' => SiteConfig::current_site_config()
            )));

        $galleryEmail->setFrom($galleryFrom)
            ->setTo($galleryTo)
            ->setSubject('Purchase request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('GalleryEMailPurchase')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork
            )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);

        $clientEmail->send();
        $galleryEmail->send();

        return Controller::redirect('thank-you/');


    }

    public function HoldForm() {

        $actionArray = array('doPurchase', 'doHold', 'doInfo');

        if( !in_array($this->request->param('Action'), $actionArray) ) {
            $artwork = Artwork::get()->filter(array("URLSegment" => $this->request->param('Action'), "ArtistID" => $this->ID))->first();
        } else {
            $artwork = false;
        }

        if(!$artwork) {
            $pageID = $this->ID;
        } else {
            $pageID = $artwork->ID;
        }

        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                //->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email"),
            //->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number"),
            //->setAttribute('required', true),
            CheckboxField::create('Confirm', 'Please hold this work for me for 48 hours')
                ->setAttribute('required', null)
                ->addExtraClass(' left-field half-field'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Name', 'Email', 'Phone', 'Confirm'
        ));

        $form = Form::create(
            $this,
            "HoldForm",
            $fields,
            new FieldList(
                FormAction::create('doHold', 'Submit')->addExtraClass("button primary")->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array("name" => "HoldCaptcha"));
    }

    public function doHold($data, Form $form) {

        $artwork = Artwork::get()->byID($data['ArtworkID']);

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        $clientEmail = new Email();
        $galleryEmail = new Email();

        $clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');


        $clientEmail->setFrom($clientFrom)
            ->setTo($data['Email'])
            ->setSubject('Hold Request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('CustomerEmailHold')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork,
                'SiteConfig' => SiteConfig::current_site_config()
            )));

        $galleryEmail->setFrom($galleryFrom)
            ->setTo($galleryTo)
            ->setSubject('Hold request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('GalleryEMailHold')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork
            )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);

        $clientEmail->send();
        $galleryEmail->send();

        return Controller::redirect('thank-you-hold/');


    }

    public function InfoForm() {

        $actionArray = array('doPurchase', 'doHold', 'doInfo');

        if( !in_array($this->request->param('Action'), $actionArray) ) {
            $artwork = Artwork::get()->filter(array("URLSegment" => $this->request->param('Action'), "ArtistID" => $this->ID))->first();
        } else {
            $artwork = false;
        }

        if(!$artwork) {
            $pageID = $this->ID;
        } else {
            $pageID = $artwork->ID;
        }

        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email (required)')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email")
                ->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number"),
            TextareaField::create('Message', 'Questions')
                ->addExtraClass('has-placeholder'),
            CheckboxField::create('Confirm', 'Please send me more info on this work')->setAttribute('required', null),
            CheckboxField::create('Subscribe', 'Please sign me up for the Oeno Gallery Newsletter'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Email', 'Confirm'
        ));

        $form = Form::create(
            $this,
            "InfoForm",
            $fields,
            new FieldList(
                FormAction::create('doInfo', 'Submit')->addExtraClass("button primary")->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array("name" => "InfoCaptcha"));
    }

    public function doInfo($data, Form $form) {
        $artwork = Artwork::get()->byID($data['ArtworkID']);

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly. ArtworkID ='.$data['ArtworkID'],
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        $galleryEmail = new Email();

        $clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');

        $galleryEmail->setFrom($galleryFrom)
            ->setTo($galleryTo)
            ->setSubject('Info request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('GalleryEMailInfo')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork
            )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);

        $galleryEmail->send();

        return Controller::redirect('thank-you-info/');

    }

    public function EmailThis(){
        $fields = FieldList::create(
            TextField::create('From', false)
                ->setAttribute('placeholder', 'Your Name')
                ->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Your Name"),
            EmailField::create('SenderEmail', false)
                ->setAttribute('placeholder', 'Your Email')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Your Email")
                ->setAttribute('required', true),
            EmailField::create('TargetEmail', false)
                ->setAttribute('placeholder', 'To (email)')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("To (email)")
                ->setAttribute('required', true),
            TextareaField::create("Message"),
            HiddenField::create('ArtworkID', null, $this->ID)
        );

        $required = new RequiredFields(array(
            'From', 'SenderEmail', 'TargetEmail'
        ));

        $form = Form::create(
            Controller::curr()->getFormOwner(),
            "EmailThis",
            $fields,
            new FieldList(
                FormAction::create('doEmail', 'Send')->addExtraClass("button primary")->setDisabled(true)
            ),
            $required
        );

        return $form->enableSpamProtection();

    }

    public function doEmail($data, Form $form) {

        //Debug::show($data);

        $artwork = Artwork::get()->byID($data['ArtworkID']);

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return $this->controller->redirectBack();
        }

        $clientEmail = new Email();


        $clientEmail->setFrom('info@oenogallery.com')
            ->setTo($data['TargetEmail'])
            ->setSubject($data['From'].' has an Artwork form Oeno Gallery')
            ->setTemplate('CustomerEmailShare')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork,
                'SiteConfig' => SiteConfig::current_site_config()
            )));


        $clientEmail->send();

        return Controller::redirect($artwork->AbsoluteLink());


    }

}
