<?php

class TaggedArtworkPage extends ArtworkHolder {

    /**
     * Static vars
     * ----------------------------------*/



    /**
     * Object vars
     * ----------------------------------*/



    /**
     * Static methods
     * ----------------------------------*/



    /**
     * Data model
     * ----------------------------------*/

    private static $db = array (
        "ArtTag" => "Varchar"
    );


    private static $has_one = array (

    );

    private static $has_many = array (

    );

    /**
     * Common methods
     * ----------------------------------*/

    public function getCMSFields()
    {
        $fields = parent::getCMSFIelds();

        $fields->insertBefore(TextField::create("ArtTag"), "Content");

        return $fields;
    }

    /**
     * Accessor methods
     * ----------------------------------*/



    /**
     * Controller actions
     * ----------------------------------*/



    /**
     * Template accessors
     * ----------------------------------*/



    /**
     * Object methods
     * ----------------------------------*/




}


class TaggedArtworkPage_Controller extends ArtworkHolder_Controller {

    private static $allowed_actions = array (
    );

    public function init() {
        parent::init();

    }

    public function RepresentedArtists() {

        return  false;
    }

    public function OtherArtists() {


        $artistList = new ArrayList();
        $artists = Artist::get();

        foreach ($artists as $artist) {
            if($artist->Artworks()->filter(
                    array("Quantity:GreaterThan" => 0, "MPTitleText3:PartialMatch" => $this->ArtTag)
                )->count() != 0) {
                $artistList->push($artist);
            }
        }

        return new PaginatedList($artistList->sort("Contact.LastName", "ASC"), $this->request);
    }


    public function PaginatedArtwork() {
        $artwork = Artwork::get()->filter(
            array("Quantity:GreaterThan" => 0, "MPTitleText3:PartialMatch" => $this->ArtTag)
        );
        $list = ArrayList::create();
        if($artwork->count()) {
            foreach ($artwork as $work) {
                $work->Name = $work->getLastName();
                $list->push($work);
            }

            return new PaginatedList($list->sort('Name'), $this->request);
        } else {
            return false;
        }
    }
}