<?php
/**
 * Class: CuratedPage
 * Summary
 * Description
 * @author: richardrudy
 * @package   * @version:
 */



class CuratedPage extends Exhibit
{
    private static $has_many = array (
        "Slides" => "Slide.ParentPage"
    );

    public function getCMSFields() {

        $fields = parent::getCMSFIelds();

        $fields->removeByName('FeaturedArtworkID');

        $slideManager = GridField::create("Slides", "Slides", $this->Slides());

        $slideManagerConfig = GridFieldConfig_RelationEditor::create();

        $slideManagerConfig->addComponent(new GridFieldOrderableRows('Sort'));

        $slideManager->setConfig($slideManagerConfig);

        $fields->addFieldToTab("Root.Slides", $slideManager);

        return $fields;
    }

    public function getCurrentSlides() {

        $today = date('Y-m-d G:i:s');


        $slides = $this->Slides();

        $activeSlides = new ArrayList();

        foreach ($slides as $slide) {

            if(!$slide->PublishDate && !$slide->UnpublishDate) { // if No dates then Push
                $activeSlides->push($slide);
            } elseif ($slide->PublishDate && $slide->PublishDate <= $today) {
                if (!$slide->UnpublishDate || $slide->UnpublishDate >= $today ) { // if start date in past and end date is future then push
                    $activeSlides->push($slide);
                }
            } elseif (!$slide->PublishDate && $slide->UnpublishDate) {
                if($slide->UnpublishDate >= $today ) { //id no start date and end date in future
                    $activeSlides->push($slide);
                }
            } elseif ($slide->PublishDate && !$slide->UnpublishDate) {
                if($slide->PublishDate <= $today) { // if start date in future and no end date
                    $activeSlides->push($slide);
                }
            }
        }


        return ($activeSlides->count()) ? $activeSlides : false;

    }
}

class CuratedPage_Controller extends Exhibit_Controller
{
    private static $allowed_actions = array (
        'index',
        'doForm',
        'SculptureForm'
    );

    public function getCTAForm() {
        return $this->SculptureForm();
    }

    public function SculptureForm() {

        $fields = FieldList::create(
            LiteralField::create('Form Header', "<h2>Thinking About a Purchase or Commission?</h2>"),
            LiteralField::create('Form Info', "<p class='text-center'>We'll get right back to you (usually within the hour).</p>"),
            TextField::create('FirstName', false)
                     ->setAttribute('placeholder', 'First Name')
                     ->addExtraClass('has-placeholder half-field left-field')
                     ->setRightTitle("First Name"),
            TextField::create('LastName', false)
                     ->setAttribute('placeholder', 'Last Name')
                     ->addExtraClass('has-placeholder half-field')
                     ->setRightTitle("Last Name"),
            EmailField::create('Email', false)
                      ->setAttribute('placeholder', 'Email (required)')
                      ->addExtraClass('has-placeholder half-field left-field')
                      ->setRightTitle("Email")
                      ->setAttribute('required', true),
            TextField::create('Phone', false)
                     ->setAttribute('placeholder', 'Phone Number')
                     ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                     ->setAttribute('title', 'A Valid International or North American Phone Number')
                     ->addExtraClass('has-placeholder half-field')
                     ->setRightTitle("Phone Number"),
            TextareaField::create('Message', 'Message (include price range and possible location)')
                         ->addExtraClass('has-placeholder')
        );

        $required = new RequiredFields(array(
            'Email'
        ));

        $form = Form::create(
            $this,
            "SculptureForm",
            $fields,
            new FieldList(
                FormAction::create('doForm', 'Submit')->addExtraClass("button primary")
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array("name" => "InfoCaptcha"));
    }


    /**
     * Process the Request Information Form
     *
     * @param $data
     * @param Form $form
     * @return bool|SS_HTTPResponse
     */
    public function doForm($data, Form $form) {
        
        $galleryEmail = new Email();

        $clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');
        $content = '';
        foreach($data as $infoKey => $infoVal) {
            if($infoVal) {
                if(is_array($infoVal)) {
                    foreach ($infoVal as $info) {
                        $content .= '<strong>' . $infoKey . '</strong> ' . $info . '<br />';
                    }
                } else {
                    $content .= '<strong>' . $infoKey . '</strong> ' . $infoVal . '<br />';
                }
            }
        }



        $galleryEmail->setFrom($galleryFrom)
                     ->setTo($galleryTo)
                     ->setSubject('Sculpture Request')->setBody($content);
                     //->setTemplate('GalleryEMailInfo')
//                     ->populateTemplate(new ArrayData(array(
//                         'Client' => $data,
//                         'Artwork' => $artwork,
//                         'Artist' => $artist
//                     )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);

        $galleryEmail->send();


        if(key_exists('Subscribe', $data) ) {
            $this->doSubscribe($data);
        }

        return Controller::redirect('thank-you-contact/');

    }

}