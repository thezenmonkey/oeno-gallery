<?php

class NewArrivalsPage extends ArtworkHolder {

    /**
     * Static vars
     * ----------------------------------*/



    /**
     * Object vars
     * ----------------------------------*/



    /**
     * Static methods
     * ----------------------------------*/



    /**
     * Data model
     * ----------------------------------*/

    private static $db = array ();


    private static $has_one = array (
        "DisplayCategory" => "ArtworkCategory"
    );

    private static $has_many = array (

    );

    /**
     * Common methods
     * ----------------------------------*/

    public function getCMSFields()
    {
        $fields = parent::getCMSFIelds();

        $fields->insertBefore(DropdownField::create("DisplayCategoryID", "Category", ArtworkCategory::get()->map('ID', 'Title')), "Content");

        return $fields;
    }

    /**
     * Accessor methods
     * ----------------------------------*/



    /**
     * Controller actions
     * ----------------------------------*/



    /**
     * Template accessors
     * ----------------------------------*/



    /**
     * Object methods
     * ----------------------------------*/




}


class NewArrivalsPage_Controller extends ArtworkHolder_Controller {

    private static $allowed_actions = array (
    );

    public function init() {
        parent::init();

    }

    public function RepresentedArtists() {

        return  false;
    }


    public function OtherArtists() {


        return  false;
    }


    public function PaginatedArtwork() {

		$artworks = Artwork::get()
                          ->filter(array("Quantity:GreaterThan" => 0, 'MPDateAdded:GreaterThan' => strtotime("-3 months")))
                          ->limit(30);
        $featured = Artwork::get()->filter(array("Featured" => 1));

        $list = ArrayList::create();
        if($artworks->count()) {
            foreach ($artworks as $artwork) {
                $list->push($artwork);
            }
        }

        if($featured->count()) {
            foreach ($featured as $artwork) {
                $list->push($artwork);
            }
        }

        if($list->count()) {
            return new PaginatedList(
                $list->sort(array("Featured" => "DESC", "MPDateAdded" => "DESC")),
                $this->request
            );
        } else {
            return false;
        }

        //$artwork = Artwork::get()->filter(array("Quantity:GreaterThan" => 0, 'MPDateAdded:GreaterThan' => strtotime("-3 months")))->sort(array("MPDateAdded" => "DESC"))->limit(30);


        //if($artwork->count()) {
        //    return new PaginatedList($artwork, $this->request);
        //} else {
        //    return false;
        //}
    }
}