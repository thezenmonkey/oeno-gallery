<?php
/**
 * Class: CategoryLandingPage.php
 * Summary
 * Description
 * @author: richardrudy
 * @package   * @version:
 */




class CategoryLandingPage extends ArtworkCategoryPage {
    private static $has_many = array (
        "Slides" => "Slide.ParentPage"
    );

    public function getCMSFields() {

        $fields = parent::getCMSFIelds();

        $slideManager = GridField::create("Slides", "Slides", $this->owner->Slides());

        $slideManagerConfig = GridFieldConfig_RelationEditor::create();

        $slideManagerConfig->addComponent(new GridFieldOrderableRows('Sort'));

        $slideManager->setConfig($slideManagerConfig);

        $fields->addFieldToTab("Root.Slides", $slideManager);

        return $fields;
    }

}

class CategoryLandingPage_Controller extends ArtworkCategoryPage_Controller {

}