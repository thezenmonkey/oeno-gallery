<?php

class VaultArtwork extends Artwork {


	private static $db = array (
		"ExtraCopy" => "HTMLText",
		"Notes" => "HTMLText"
	);

	private static $belongs_many_many = array(
		'Members' => 'Member'
	);

	public function getCMSFields()
	{

		$fields = parent::getCMSFIelds();

		$fields->insertBefore( HtmlEditorField::create("ExtraCopy"), "Price" );
		$fields->insertAfter( HtmlEditorField::create( "Notes", "Notes <strong style='color:#000'>*Private*</strong>" ), "FrameOptions" );

		return $fields;
	}


}
