<?php
/**
 * Class: CuratedPage
 * Summary
 * Description
 * @author: richardrudy
 * @package   * @version:
 */



class CuratedPageFormless extends CuratedPage
{

}

class CuratedPageFormless_Controller extends CuratedPage_Controller
{
    private static $allowed_actions = array (
        'index',
        'doForm',
        'SculptureForm'
    );

    public function getCTAForm() {
        return false;
    }


}