<?php
use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\EmailMarketing\Campaign;
use Ctct\Exceptions\CtctException;
use Ctct\Services\ListService;
use Ctct\Services\EmailMarketingService;

class SubscriptionPage extends Page {

    /**
     * Static vars
     * ----------------------------------*/



    /**
     * Object vars
     * ----------------------------------*/



    /**
     * Static methods
     * ----------------------------------*/



    /**
     * Data model
     * ----------------------------------*/

    private static $db = array (

    );


    private static $has_one = array (

    );

    private static $has_many = array (

    );

    /**
     * Common methods
     * ----------------------------------*/



    /**
     * Accessor methods
     * ----------------------------------*/



    /**
     * Controller actions
     * ----------------------------------*/



    /**
     * Template accessors
     * ----------------------------------*/


    /**
     * Object methods
     * ----------------------------------*/




}


class SubscriptionPage_Controller extends Page_Controller {

    private static $allowed_actions = array (
        'SubscribeForm'
    );

    private static $apikey = 'd5fb0a11b027730f0c7eb82ec73ebf8b-us13';

    private static $campaignList;
    private static $ccConnection;

    public function init() {
        parent::init();
    }

    public function SubscribeForm() {

//        $cclists = array(
//            1334246807 => "I am a designer/ architect and looking for work for clients.",
//            1590821836 => "I am a serious collector.",
//            1090672915 => "I am an artist.",
//            1323229444 => "I am just browsing but love art.",
//            1949471192 => "I am planning on buying art in the next year.",
//            2066679782 => "I hope to visit the gallery soon.",
//            1627381785 => "I visited the gallery."
//        );
//
//        $lists = array(
//            '0b960d011f' => "I am a designer/ architect and looking for work for clients.",
//            'aa982a7814' => "I am a serious collector.",
//            '3fbca3713a' => "I am an artist.",
//            'c0ef407e3e' => "I am just browsing but love art.",
//            'c55854cd44' => "I am planning on buying art in the next year.",
//            'e3ac5069ea' => "I hope to visit the gallery soon.",
//            'da5fa95c7a' => "I visited the gallery."
//        );

        $lists = array(
            '9a2a24c8e3' => "I am a designer/ architect and looking for work for clients.", //list 3379628662 designers
            'aa982a7814' => "I am a serious collector.",
            '06d3d00c12' => "I am an artist.",                                              // list 455a032b34
            '6bc7f05108' => "I am just browsing but love art.",                             // list 273e7a8620
            'c55854cd44' => "I am planning on buying art in the next year.",
            '6022bca86c' => "I hope to visit the gallery soon.",                            // list 273e7a8620
            '4dca6d54ee' => "I visited the gallery."                                        // list 273e7a8620
        );

        //a996f826-dd7a-4a17-8b52-8d81dd55e074

        $fields = new FieldList(
            TextField::create("FirstName", false)
                ->setAttribute('placeholder', 'First Name')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First Name"),
            TextField::create("LastName", false)
                ->setAttribute('placeholder', 'Last Name')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Last Name"),
            EmailField::create("Email", false)
                ->setAttribute('placeholder', 'Email Address')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Email Address"),
            CountryDropdownField::create("Country"),
            CheckboxSetField::create('Lists', 'Which statements best describe you (check all that apply)', $lists)

        );
        $actions = new FieldList(
            FormAction::create("doMCSubscribe")->setTitle("Subscribe")->addExtraClass("button primary")//->setDisabled(true)
        );

        $required = new RequiredFields(array(
            'FirstName', 'LastName', 'Email', 'Lists'
        ));

        $form = new Form($this, 'SubscribeForm', $fields, $actions, $required);

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form;//->enableSpamProtection(array("name" => "HoldCaptcha"));
    }

    public function doMCSubscribe(array $data, Form $form) {

        $interestList = null;

        $lists = array(
            '9a2a24c8e3' => "I am a designer/ architect and looking for work for clients.", //list 3379628662 designers
            'b03ea2c15f' => "I am a serious collector.",                                    // list 273e7a8620
            '06d3d00c12' => "I am an artist.",                                              // list 455a032b34
            '6bc7f05108' => "I am just browsing but love art.",                             // list 273e7a8620
            '6bc7f05108' => "I am planning on buying art in the next year.",
            '6022bca86c' => "I hope to visit the gallery soon.",                            // list 273e7a8620
            '4dca6d54ee' => "I visited the gallery."                                        // list 273e7a8620
        );

        $designerArray = array('9a2a24c8e3');
        $generalArray = array('6bc7f05108', '6022bca86c', '4dca6d54ee', 'b03ea2c15f');
        $artistArray = array('455a032b34');

        $designInterest = array();
        $generalInterest = array();
        $artistInterest = array();


        if(count($data['Lists'] > 0)) {
            foreach($data['Lists'] as $interest) {

                if(in_array($interest, $designerArray)) {
                    $designInterest[$interest] = true;
                }
                if(in_array($interest, $generalArray)) {
                    $generalInterest[$interest] = true;
                }
                if(in_array($interest, $artistArray)) {
                    $artistInterest[$interest] = true;
                }
            }
        }

        $auth = base64_encode( 'user:d5fb0a11b027730f0c7eb82ec73ebf8b-us13' );


        //Check if email exists
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/3379628662/members/'. md5($data['Email']));
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
//            'Authorization: Basic '.$auth));
//        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
//        curl_setopt($ch, CURLOPT_POST, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        $result = curl_exec($ch);
//        $json = json_decode($result);

        if($designInterest && count($designInterest)) {

            $json['design']['check'] = $this->checkMCEmail('3379628662', $data['Email'], $auth);
            $json['design']['list'] = '3379628662';
            $json['design']['interests'] = $designInterest;
        }

        if($generalInterest && count($generalInterest)) {
            $json['general']['check'] = $this->checkMCEmail('273e7a8620', $data['Email'], $auth);
            $json['general']['list'] = '273e7a8620';
            $json['general']['interests'] = $generalInterest;
        }

        if($artistInterest && count($artistInterest)) {
            $json['artist']['check'] = $this->checkMCEmail('455a032b34', $data['Email'], $auth);
            $json['artist']['list'] = '455a032b34';
            $json['artist']['interests'] = $artistInterest;
        }


        foreach ($json as $emailCheck) {
            $subList = $emailCheck['list'];

            if($emailCheck['check']->{'status'} == 404) {
                // if no member add to list



                $submitdata = array(
                    'apikey'        => 'd5fb0a11b027730f0c7eb82ec73ebf8b-us13',
                    'email_address' => $data['Email'],
                    'status'        => 'subscribed',
                    'merge_fields'  => array(
                        'FNAME' => $data['FirstName'],
                        'LNAME' => $data['LastName']
                    ),
                    'interests'     => $emailCheck['interests']
                );
                $json_data = json_encode($submitdata);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://us13.api.mailchimp.com/3.0/lists/$subList/members/");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                    'Authorization: Basic '.$auth));
                curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                $result = curl_exec($ch);
                $json = json_decode($result);
                $mcResult[] = $json->{'status'};
            } elseif ($emailCheck['check']->{'status'} == 400 && $emailCheck['check']->{'title'} == 'Member Exists') {

                //if member exist then update
                $submitdata = array(
                    'apikey'        => 'd5fb0a11b027730f0c7eb82ec73ebf8b-us13',
                    //'email_address' => $data['Email'],
                    //'status'        => 'subscribed',
                    'merge_fields'  => array(
                        'FNAME' => $data['FirstName'],
                        'LNAME' => $data['LastName']
                    ),
                    'interests'     => $emailCheck['interests']
                );

                $json_data = json_encode($submitdata);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://us13.api.mailchimp.com/3.0/lists/$subList/members/". md5($data['Email']));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                    'Authorization: Basic '.$auth));
                curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                $result = curl_exec($ch);
                $json = json_decode($result);
                $mcResult[] = $json->{'status'};
            }
        }



//        if($json->{'status'} == 'subscribed') {
            Controller::redirect('subscribe/thank-you');
//        } else {
//            $form->addErrorMessage('Message', 'There was a problem with your request. - '.$json->{'status'}, 'error');
//            Controller::curr()->redirectBack();
//        }


    }

    public function checkMCEmail($list, $email, $auth) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://us13.api.mailchimp.com/3.0/lists/$list/members/". md5($email));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $json = json_decode($result);

        return $json;
    }

    public function doSubscribe(array $data, Form $form) {
        $cc = $this->getccConnection();

        // attempt to fetch lists in the account, catching any exceptions and printing the errors to screen
        try {
            $lists = $cc->listService->getLists(CCACCESS_TOKEN);
        } catch (CtctException $ex) {
            foreach ($ex->getErrors() as $error) {
                print_r($error);
            }
            if (!isset($lists)) {
                $lists = null;
            }
        }

        try {
            // check to see if a contact with the email address already exists in the account
            $response = $cc->contactService->getContacts(CCACCESS_TOKEN, array("email" => $data['Email']));

            // create a new contact if one does not exist
            if (empty($response->results)) {
                $action = "Creating Contact";

                $contact = new Contact();
                $contact->addEmail($data['Email']);
                foreach($data['Lists'] as $list) {
                    $contact->addList($list);
                }
                $contact->first_name = $data['FirstName'];
                $contact->last_name = $data['LastName'];
                if(array_key_exists('Country', $data)){
                    $contact->addresses = array(
                        array(
                            "address_type" => "PERSONAL",
                            'country_code' => $data['Country']
                        )

                    );
                }

                /*
                 * The third parameter of addContact defaults to false, but if this were set to true
                 * it would tell Constant Contact that this action is being performed by the contact themselves,
                 * and gives the ability to opt contacts back in and trigger Welcome/Change-of-interest emails.
                 *
                 * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                 */
                $returnContact = $cc->contactService->addContact(CCACCESS_TOKEN, $contact);

                //$form->addErrorMessage('Message', 'Thank you for subscribing, please check you email to confirm subscription', 'success');
                Controller::redirect('subscribe/thank-you');


                // update the existing contact if address already existed
            } else {
                $action = "Updating Contact";

                $contact = $response->results[0];
                if ($contact instanceof Contact) {
                    foreach($data['Lists'] as $list) {
                        $contact->addList($list);
                    }
                    $contact->first_name = $data['FirstName'];
                    $contact->last_name = $data['LastName'];
                    if(array_key_exists('Country', $data)){
                        $contact->addresses = array(
                            array(
                                "address_type" => "PERSONAL",
                                'country_code' => $data['Country']
                            )

                        );
                    }


                    /*
                     * The third parameter of updateContact defaults to false, but if this were set to true it would tell
                     * Constant Contact that this action is being performed by the contact themselves, and gives the ability to
                     * opt contacts back in and trigger Welcome/Change-of-interest emails.
                     *
                     * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                     */
                    $returnContact = $cc->contactService->updateContact(CCACCESS_TOKEN, $contact);

                    //$form->addErrorMessage('Message', 'Thank you for subscribing', 'success');
                    Controller::redirect('subscribe/thank-you');
                } else {
                    $e = new CtctException();
                    $e->setErrors(array("type", "Contact type not returned"));
                    throw $e;
                }
            }

            // catch any exceptions thrown during the process and print the errors to screen
        } catch (CtctException $ex) {
            $form->addErrorMessage('Message', 'There was a problem with your request.', 'error');
            Controller::curr()->redirectBack();
//            echo '<span class="label label-important">Error ' . $action . '</span>';
//            echo '<div class="container alert-error"><pre class="failure-pre">';
//            print_r($ex->getErrors());
//            echo '</pre></div>';
//            die();
        }

    }

    public function EmailListCacheKey() {
        $key = (int)(time() / 60 / 60 / 24 / 14 );
        return $key;
    }

    public function getCampaignList() {
        if(!isset(self::$campaignList)) {
            $list = $this->getEmailCampaignIDs();
            if($list) {
                self::$campaignList = $list;
            }
        }
        return self::$campaignList;
    }

    /**
     * Check if Constant Contact Campaign Name matches the Searched Filter
     * @param $filter String to search for in the Name
     * @return array
     */
    public function filterEmailLinks($filter) {
        $campaignList = $this->getCampaignList();

        $emailList = array();

        if(count($campaignList)) {
            foreach($campaignList as $campaign) {
                if($this->checkCampaign($campaign, $filter)) {
                    //$emailList->push($campaign);
                    array_push($emailList, $campaign);
                }
            }
        }

        return $emailList;

    }

    public function FocalPointDesignersList() {
        $emailList =  $this->filterEmailLinks("Focal Point");

        $approvedEmailIDs = array(
            '1122208772101', //FP Sept 201
            '1121508331072', //FP Aug 2015
            '1120899326541', //FP May 2015
            '1120573941307', //FP April 2015
            '1120215896127' //FP Feb 2015
        );

        if (count($emailList) < 5) {
            $i = 0;
            while (count($emailList) < 5 ) {
                $campaign = $this->getEmailCampaign($approvedEmailIDs[$i]);

                if($campaign) {
                    array_push($emailList, $campaign);
                }

                $i++;
            }
        }

        $frontEndList = new ArrayList();

        foreach ($emailList as $email) {
            $frontEndList->push($email);
        }

        return $frontEndList;
    }

    public function NewslettersList() {
        $emailList = $this->filterEmailLinks("Oeno Newsletter");

        $approvedEmailIDs = array(
            1122538884914, //Collett Langstroth Clifford
            1122062347928, //Otto Rogers Exhibition
            1121924579566, //August Newsletter
            1121415164494, //Stargazing
            1121118473869 //John Fox Invite
        );

        if (count($emailList) < 5) {
            $i = 0;
            while (count($emailList) < 5 ) {
                $campaign = $this->getEmailCampaign($approvedEmailIDs[$i]);

                if($campaign) {
                    array_push($emailList, $campaign);
                }

                $i++;
            }
        }

        $frontEndList = new ArrayList();

        foreach ($emailList as $email) {
            $frontEndList->push($email);
        }

        return $frontEndList;
    }


    /**
     * Get a Constant Contact Campaign by ID
     * @param $ID ID of Campaign
     * @return ArrayData|bool ArrayData of Title and Link for Campaign
     */
    public function getEmailCampaign($ID) {
        $cc = $this->getccConnection();

        try {
            // check to see if a contact with the email address already exists in the account
            $response = $cc->emailMarketingService->getCampaign(CCACCESS_TOKEN, $ID);

            if($response && strlen($response->permalink_url) != 0) {

                $campaignData = new ArrayData(array(
                    "Title" => $response->subject,
                    "Link" => $response->permalink_url
                ));

                return $campaignData;
            } else {
                return false;
            }


        } catch (CtctException $ex) {

            echo '<span class="label label-important">Error ' . $action . '</span>';
            echo '<div class="container alert-error"><pre class="failure-pre">';
            print_r($ex->getErrors());
            echo '</pre></div>';
            return false;
        }


    }


    /**
     * Get a List of Campaigns from Constant Contact (2 weeks old)
     * @return array|bool Constant Contact Campaign List or false
     */
    public function getEmailCampaignIDs() {

        $cc = $this->getccConnection();

        try {
            // check to see if a contact with the email address already exists in the account
            $params = array(
                'modified_since' => date('Y-m-d', strtotime('-2 weeks'))
            );
            $response = $cc->emailMarketingService->getCampaigns(CCACCESS_TOKEN, $params);

            if($response) {

                return $response->results;
            } else {
                return false;
            }


        } catch (CtctException $ex) {

            echo '<span class="label label-important">Error ' . $action . '</span>';
            echo '<div class="container alert-error"><pre class="failure-pre">';
            print_r($ex->getErrors());
            echo '</pre></div>';
            return false;
        }
    }


    /**
     * @param $campaign array Constant Contact Email Campaign
     * @param $filter  string Campaign name to search for
     * @return bool true if Campaign Name matched $filter
     */
    public function checkCampaign($campaign, $filter) {

        if($campaign->status == "SENT") {
            if(strpos(strtolower($campaign->name), strtolower($filter)) === false) {
                return false;
            } else {
                return true;
            }
        }

        return false;

    }


    /**
     * Create a New Constant Contact Connection and Save to Static
     * @return bool|ConstantContact Constant Contact Connection
     */
    public function getccConnection() {
        if(!isset(self::$ccConnection)) {
            $cc = new ConstantContact(CCAPIKEY);

            // attempt to fetch lists in the account, catching any exceptions and printing the errors to screen
            try {
                $lists = $cc->listService->getLists(CCACCESS_TOKEN);

                if($lists) {
                    self::$ccConnection = $cc;
                }

            } catch (CtctException $ex) {
                foreach ($ex->getErrors() as $error) {
                    print_r($error);
                }
                if (!isset($lists)) {
                    $lists = null;
                }

                return false;
            }

        }
        return self::$ccConnection;
    }

}