<?php



class Slide extends DataObject implements PermissionProvider {
	
	private static $db = array(
	    "CustomIntro" => "Varchar(255)",
	    "CustomTitle" => "Varchar(255)",
        "CustomSubTitle" => "Varchar(255)",
        "PublishDate" => "SS_Datetime",
        "UnpublishDate" => "SS_Datetime",

        "Sort" => "Int"
    );

	private static $has_one = array(
	    "Artwork" => "Artwork",
        "ParentPage" => "SiteTree",
        "LargeBackground" => "Image",
        "TargetPage" => "SiteTree"
    );

	private static $summary_fields = array (
	    "SlideTitle"
    );

	private static $default_sort = array(
	    'Sort'
    );

    public function providePermissions()
    {
        return array(
            'MANAGE_SLIDES' => 'Manage Slide',
        );
    }
    /**
     * @param null $member
     * @return boolean
     */
    public function canCreate($member = null)
    {
        return Permission::check('MANAGE_SLIDES');
    }
    /**
     * @param null $member
     * @return boolean
     */
    public function canDelete($member = null)
    {
        return Permission::check('MANAGE_SLIDES');
    }
    /**
     * @param null $member
     * @return boolean
     */
    public function canEdit($member = null)
    {
        return Permission::check('MANAGE_SLIDES');
    }
    /**
     * @param null $member
     * @return boolean
     */
    public function canView($member = null)
    {
        return Permission::check('MANAGE_SLIDES');
    }

    public function getCMSFields() {
        $fields = parent::getCMSFIelds();

//        $fields->addFieldToTab("Root.Main", DropdownField::create(
//            "ArtworkID",
//            "Artwork",
//            Artwork::get()->sort('Created', 'DESC')->limit(10)->map()
//        )->setEmptyString('(Select one)'));


        $fields->addFieldToTab("Root.Main", new HasOnePickerField($this, 'ArtworkID', 'Selected Artwork', $this->Artwork(), 'Select an Artwork'));

        $fields->addFieldToTab("Root.Main", TreeDropdownField::create("TargetPageID", "Target Page", "SiteTree"));

        return $fields;
    }



    public function ImageLarge() {
        if($this->LargeBackgroundID != 0) {
            return $this->LargeBackground()->FileName;
        } elseif ($this->ArtworkID != 0) {
            return ($this->Artwork()->OrderedImages()->First()->FileName);
        }

        return false;
    }

    public function ImageMedium() {
        if($this->LargeBackgroundID != 0) {
            return $this->LargeBackground()->croppedImage(800, 600)->FileName;
        } elseif ($this->ArtworkID != 0) {
            return ($this->Artwork()->OrderedImages()->First()->croppedImage(800, 600)->FileName);
        }

        return false;
    }

    public function ImageSmall() {
        if($this->LargeBackgroundID != 0) {
            return $this->LargeBackground()->croppedImage(360, 480)->FileName;
        } elseif ($this->ArtworkID != 0) {
            return ($this->Artwork()->OrderedImages()->First()->croppedImage(360, 480)->FileName);
        }

        return false;
    }

    public function getIntro() {
        if($this->CustomIntro) {
            return $this->CustomIntro;
        } else {
            if($this->TargetPage()->ClassName == 'Exhibit') {
                if($this->TargetPage()->StartDate > date('Y-m-d')) {
                    return "Upcoming Show";
                } else {
                    return "Now Showing";
                }
            }
        }

        return false;
    }


    public function getTitle() {

        if($this->CustomTitle) {
            return $this->CustomTitle;
        } else {
            if($this->ArtworkID != 0) {
                return $this->Artwork()->Title;
            } elseif ($this->TargetPageID != 0) {
                return $this->TargetPage()->Title;
            }
        }

        return false;
    }



    public function getSubTitle() {
        if($this->CustomSubTitle) {
            return $this->CustomSubTitle;
        } else {
            if ($this->ArtworkID != 0 ){
                return $this->Artwork()->Artist()->Title;
            } elseif ($this->TargetPageID != 0) {
                $page = $this->TargetPage();
                if($page->ClassName == 'Exhibit') {
                    return $page->obj('StartDate')->Long().'&mdash;'.$page->obj('EndDate')->Long();
                } else {
                    return 'Read More';
                }



            }
        }

        return false;
    }

    public function getSlideTitle() {
        if($this->ArtworkID != 0) {
            return $this->Artwork()->Title.' - Artwork';
        } elseif ($this->TargetPageID != 0) {
            return $this->TargetPage()->Title.' - '.$this->TargetPage()->ClassName;
        } elseif ($this->CustomTitle) {
            return $this->CustomTitle;
        } else {
            return $this->CustomIntro;
        }

        return false;
    }
}