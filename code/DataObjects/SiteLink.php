<?php

class SiteLink extends DataObject {
	
	private static $db = array (
		"Title" => "Varchar(255)",
		"URL" => "Varchar(255)",
		"Description" => "Text"
	);
	
	private static $has_one = array (
		"SiteConfig" => "SiteConfig",
		"LinkCategory" => "LinkCategory"
	);
	
	private static $has_many = array (
		
	);
	
}

class LinkCategory extends Category {
	private static $has_many = array(
		"Links" => "SiteLink"	
	);
}