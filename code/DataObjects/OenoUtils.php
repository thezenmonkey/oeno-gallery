<?php
	
class OenoUtils extends Extension {
	
	public static function ArtistTags($tags) {
		// find no price tag
		if( stripos($tags->MPDescription,"@no-price@") !== false ) {
			$tags->NoPrice = 1;
		}
		
		
		//find video tag
		if( stripos($tags->MPDescription,"@video-start@") !== false ) {
			echo $tags->Title."<br>";
			$pattern = "/@video-start@(.*?)@video-end@/";
			preg_match_all($pattern, $tags->MPDescription, $matches);
			
			$i = 1;
			foreach ($matches[1] as $videoURL) {
				$video = new Video();
				$video->Title = $tags->Title." ".$i;
				$video->URL = $videoURL;
				$video->ArtistID = $tags->ID;
				
				$video->write();
				$i++;
			}
			
		}
		
		//find secondary tag
		if( stripos($tags->MPDescription,"@secondary@") !== false ) {
			$tags->Secondary = 1;
		}
		
		//find secondary tag
		if( stripos($tags->MPDescription,"@outdoor@") !== false ) {
			$tags->Outdoor = 1;
		}
		
	}
	
	
}