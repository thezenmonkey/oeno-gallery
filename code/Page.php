<?php
<?php
class Page extends SiteTree {

	private static $db = array(
	);

	private static $has_one = array(
	);

	public function getCMSFields() {
		$fields = parent::getCMSFIelds();

		$uniquePages = array(
			'Artwork',
			'Artist',
			'Exhibit',
			'ArtistsPage',
			'ArtworkHolder'
		);

		if( !in_array($this->ClassName, $uniquePages) ) {
			$fields->addFieldsToTab("Root.Main", array(
				TextField::create("HeaderClass"),
				UploadField::create("HeaderImage")
			));
		}

		return $fields;
	}

}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		"IsLive", "IsDev", "SearchForm", "SearchArt", "doArtSearch", "PlusSpacer"
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements



	}

	function index() { // index runs if no other function is being called - it is like a second init()
        if($this->getRequest()->isAjax() && !in_array(
                $this->ClassName, array("ArtistsPage","ArtworkHolder","TaggedArtworkPage","HistoricalArtistsPage", "ArtworkCategoryPage"))
        ) {
            return $this->renderWith(array('AjaxPage', 'Page'));
		} else {
			return Array();
		}
	}

	function IsLive() {
		return Director::isLive() ? true : false;
	}

	function IsDev() {
		return Director::isDev() ? true : false;
	}


	public function SearchArt() {
        $context = singleton('Artwork')->getCustomSearchContext();

        $fields = FieldList::create(

	        TextField::create("Keywords", '')
                ->setAttribute('placeholder', 'Artist Name or Keyword (comma separated)')
                ->addExtraClass('has-placeholder'),
	        FormAction::create('doArtSearch', 'Search Artwork')->addExtraClass("btn btn-primary")
        );

        $form = Form::create(
        	$this,
        	"SearchArt",
            $fields,
            new FieldList(
                //FormAction::create('doArtSearch', 'Search Artwork')->addExtraClass("btn btn-primary")
            )
        )->disableSecurityToken();
        return $form->loadDataFrom($this->request->postVars());
    }

    public function doArtSearch($data, $form, $request) {

		$redirect = SiteTree::get_by_link("find-art");
		$this->redirect($redirect->Link().'SearchArt?'.http_build_query($this->request->postVars('Keywords')), 301);
		return;

    }

    public function PlusSpacer($val) {
	    return str_ireplace(' ', '+', $val);
    }

	public function SearchRequest() {
		if($this->request->postVars('Keywords')) {
			return true;
		} else {
			return false;
		}
	}

	public function setMessage($type, $message) {
        Session::set('Message', array(
            'MessageType' => $type,
            'Message' => $message
        ));
    }

    public function getMessage() {
        if($message = Session::get('Message')){
            Session::clear('Message');
            $array = new ArrayData($message);
            return $array->renderWith('Message');
        }
        return false;
    }

	public function CurrentMember() {
		if($member = Member::currentUser()) {
			return $member;
		} else {
			return false;
		}
	}

/*
	 public function SearchForm() {

        $fields = new FieldList(
	        TextField::create("Search", '')
        );

        $form = new Form($this, "SearchForm",
            $fields,
            new FieldList(
                new FormAction('doSearch', "Search")
            )
        );

        return $form;
    }
*/

}
