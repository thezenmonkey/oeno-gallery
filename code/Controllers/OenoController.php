<?php
	
class OenoController extends Controller {
	
	private static $allowed_actions = array(
		"ArtistRedirector", "ExhibitRedirector", "CategoryRedirector", 'RedirectLogic'
	);
	
	private static $url_handlers = array(
/*
		'artist//$ID' => "ArtistRedirector",
		'exhibit//$ID' => "ExhibitRedirector",
		'fltr//$ID' => "CategoryRedirector",
*/
		'$Slug!' => 'RedirectLogic'
	);
	
	
	public function init() {
		parent::init();
		$request = $this->request;
		
		if($request->getVar('ai')) {
			$artist = Artist::get()->filter(array("MPArtistID" => $request->getVar('ai')))->first();
			if($artist) {
				return $this->redirect($artist->Link(), 301);
			} else {
				$redirect = SiteTree::get_by_link("artists");
				return $this->redirect($redirect->Link(), 301);
			}
		} elseif ($request->getVar('tag')) {
			$exhibit = Exhibit::get()->filter(array("MPFeatured:PartialMatch" => '@'.$request->getVar('tag').'@'))->first();
			
			if($exhibit) {
				return $this->redirect($exhibit->Link(), 301);
				
			} else {
				$redirect = SiteTree::get_by_link("exhibitions");
				return $this->redirect($redirect->Link(), 301);
			}
		} elseif ($request->getVar('fltr')) {
			$cat = ArtworkCategory::get()->filter(array("Title" => $request->getVar('fltr')))->first();
			$redirect = SiteTree::get_by_link("find-art");
			if($cat) {
				return $this->redirect($redirect->Link().'SearchArt?'.http_build_query(array("CategoryID" => $cat->ID)).'&action_doArtSearch=Search+Artwork', 301);
			} else {
				return $this->redirect($redirect->Link(), 301);
			}
		}
		
		$redirect = DataObject::get_one('ErrorPage', '"ErrorPage"."ErrorCode" = \'404\'');
		return $this->redirect($redirect->Link(), 404);
		
		
	}
	
	public function RedirectLogic(SS_HTTPRequest $request) {
		//URL Segment Artitst ?ai=MPArtistID
		
		Debug::show($request);
		if($request->getVar('ai')) {
			$artist = Artist::get()->filter(array("MPArtistID" => $request->getVar('ai')))->first();
			
			if($artist) {
				//return $this->redirect($artist->Link(), 301);
				
			}
		}
		$redirect = SiteTree::get_by_link("artists");
		//return $this->redirect($redirect->Link(), 301);
	}
	
	
	public function ArtistRedirector(SS_HTTPRequest $request) {
		//URL Segment Artitst ?ai=MPArtistID
		if($request->getVar('ai')) {
			$artist = Artist::get()->filter(array("MPArtistID" => $request->getVar('ai')))->first();
			
			if($artist) {
				return $this->redirect($artist->Link(), 301);
				
			}
		}
		$redirect = SiteTree::get_by_link("artists");
		return $this->redirect($redirect->Link(), 301);
	}
	
	public function ExhibitRedirector(SS_HTTPRequest $request) {
		//URL Segment Exhibit ?tag=MPFeatured
		if($request->getVar('tag')) {
			$exhibit = Exhibit::get()->filter(array("MPFeatured:PartialMatch" => '@'.$request->getVar('tag').'@'))->first();
			
			if($exhibit) {
				return $this->redirect($exhibit->Link(), 301);
				
			}
		}
		$redirect = SiteTree::get_by_link("exhibitions");
		return $this->redirect($redirect->Link(), 301);
		
	}
	
	public function CategoryRedirector(SS_HTTPRequest $request) {
		//URL Segment fltr ?fltr=ArtworkCategory.Title
		$redirect = SiteTree::get_by_link("find-art");
		if($request->getVar('fltr')) {
			$cat = ArtworkCategory::get()->filter(array("Title" => $request->getVar('fltr')))->first();
			
			if($cat) {
				return $this->redirect($redirect->Link().'SearchArt?'.http_build_query(array("CategoryID" => $cat->ID)), 301);
				
			}
		}
		
		return $this->redirect($redirect->Link(), 301);
	}
	
}