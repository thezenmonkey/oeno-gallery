<?php

class OenoArtist extends Extension {

	private static $db = array(
		"NoPrice" => "Boolean",
		"Outdoor" => "Boolean",
		"Secondary" => "Boolean",
		"PictureAlign" => "Enum(array('align-left', 'align-right'))",
		"BioTop" => "Boolean",
		"ManualSort" => "Boolean",
        "Historical" => "Boolean",
		"USD" => "Boolean"
	);

	private static $has_one = array(

	);

	private static $has_many = array(
		"VaultArtworks" => "VaultArtwork"
	);

    private static $many_many = array(
        "RelatedArtists" => "Artist"
    );

    private static $belongs_many_many = array(
        "OtherArtists" => "Artist"
    );

	private static $defaults = array(
        "PictureAlign" => 'align-left',
    );

	public function updateCMSFields(FieldList $fields) {

		$fields->insertAfter(CheckboxField::create("NoPrice")->setDescription('Do not show price for artwork'), "Title");
		$fields->insertAfter(CheckboxField::create("USD"),"NoPrice");
		$fields->insertAfter(CheckboxField::create("BioTop")->setDescription('Move Short Bio to Top of Page'), "Title");
        $fields->insertAfter(CheckboxField::create("Historical"), "Title");
		$fields->addFieldToTab("Root.Main", DropdownField::create("PictureAlign", "Align Picture", singleton('Artist')->dbObject('PictureAlign')->enumValues())->setDescription("Align Artis's Picture to Left or Right of Bio"));
		$fields->addFieldToTab("Root.Artwork", CheckboxField::create("ManualSort")->setDescription('Sort Artwork Manually on Website (default by date added)'));

        $relationFieldConfig = GridFieldConfig_RelationEditor::create();
        $relationFieldConfig->removeComponentsByType('GridFieldAddNewButton');
        $relationField = GridField::create('RelatedArtists', "Related Artists", $this->owner->RelatedArtists());
        $relationField->setConfig($relationFieldConfig);

        $fields->addFieldToTab("Root.RelatedArtists", $relationField);

        if($this->owner->OtherArtists()->count()) {
            $otherArtistsList = "<ul>";

            foreach($this->owner->OtherArtists() as $artist) {
                $otherArtistsList .= "<li>".$artist->Title."</li>";
            }

            $otherArtistsList .= "</ul>";

            $fields->addFieldsToTab("Root.RelatedArtists", array(
                    HeaderField::create("OtherArtistsHeader", "Other Artists", 2),
                    LiteralField::create("OtherArtistsDesc", "<p>Artists using ".$this->owner->Title." as a related artist</p>"),
                    LiteralField::create("OtherArtistsList", $otherArtistsList)
                )
            );
        }

		if($this->owner->ID != 0) {

			$vaultArtworkFieldConfig = GridFieldConfig_RelationEditor::create();
			$vaultArtworkFieldConfig->addComponent( new GridFieldOrderableRows('Sort') );
			$vaultArtworkFieldConfig->removeComponentsByType( 'GridFieldDeleteAction' );
			$vaultArtworkFieldConfig->addComponent( new GridFieldDeleteAction() );
			$vaultArtworkFieldConfig->removeComponentsByType( 'GridFieldAddExistingAutocompleter' );

			$vaultArtworkField = GridField::create("VaultArtworks", "VaultArtworks",$this->owner->VaultArtworks());
			$vaultArtworkField->setConfig($vaultArtworkFieldConfig);

			$fields->addFieldsToTab("Root.Vault", $vaultArtworkField);

		}
	}

	public function ArtworkSortField() {
		return $this->owner->ManualSort ? "Sort" : "MPDateAdded";
	}

	public function ArtworkSortDirection() {
		return $this->owner->ManualSort ? "ASC" : "DESC";
	}

	public function OrderedImages() {
		if($this->owner->PictureID != 0) {
			$list = new ArrayList();
			$list->push($this->owner->Picture());

			return $list;
		} else {
			return false;
		}
	}

    public function RelatedArtistsList($count = 4) {
        if($this->owner->RelatedArtists()->count()) {
            return $this->owner->RelatedArtists()->limit($count);
        } elseif ($this->owner->OtherArtists()->count()) {
            return $this->owner->OtherArtists()->limit($count);
        } else {
            return false;
        }
    }

    public function ArtistShows() {
	    $artIDArray = $this->owner->Artworks()->getIDList();
	    $artIDs = join(",", array_keys($artIDArray));

        $exhibitQuery = new SQLQuery();
        $exhibitQuery->setFrom('Exhibit_Artworks');
        $exhibitQuery->setSelect('ExhibitID');
        $exhibitQuery->addWhere("ArtworkID in ($artIDs)");

        $exhibitQuery->addGroupBy('ExhibitID');

        $result = $exhibitQuery->execute();

        $exhibitIDs = array();
        $shows = '';

        foreach($result as $row) {
            $exhibitIDs[] = $row['ExhibitID'];
        }

        if(count($exhibitIDs)) {
            $shows = Exhibit::get()->byIDs($exhibitIDs);
        }

	    return ($shows && $shows->count()) ? $shows->sort('StartDate', 'DESC') : false;
    }

}


class OenoSiteTree extends DataExtension {

	private static $db = array(
		"HeaderClass" => "Varchar(255)"
	);

	private static $has_one = array(
		"HeaderImage" => "Image"
	);

}


class OenoHomePage extends DataExtension {

	private static $many_many = array (
		"Images" => "Image",
		//"Tags" => "ArtworkTag"
	);

	private static $has_many = array (
	    "Slides" => "Slide.ParentPage"
    );

	public function updateCMSFields( FieldList $fields) {
	    $slideManager = GridField::create("Slides", "Slides", $this->owner->Slides());

	    $slideManagerConfig = GridFieldConfig_RelationEditor::create();

	    $slideManagerConfig->addComponent(new GridFieldOrderableRows('Sort'));

	    $slideManager->setConfig($slideManagerConfig);

	    $fields->addFieldToTab("Root.Slides", $slideManager);
    }

	public function OenoLatest($limit = 6) {

		$artworks = Artwork::get()->filter(array('Quantity:GreaterThan' => 0, 'MPTitleText1:PartialMatch' => '@new@'))->sort("MPDateAdded", "DESC")->limit($limit);

		return $artworks->count() ? $artworks : false;

	}

	public function CurrentExhibition() {
		$today = date('Y-m-d');

		$exhibition = Exhibit::get()->filter(
			array(
				"StartDate:LessThanOrEqual" => $today,
				"EndDate:GreaterThanOrEqual" => $today
			)
		)->sort(array("OnGoing" => "AsC", "StartDate" => "DESC"))->first();
		return ($exhibition) ? $exhibition : false;
	}

	public function CurrentExhibitions() {
		$today = date('Y-m-d');

		$exhibition = Exhibit::get()->filter(
			array(
				"StartDate:LessThanOrEqual" => $today,
				"EndDate:GreaterThanOrEqual" => $today
			)
		)->sort(array("OnGoing" => "AsC", "StartDate" => "DESC"));
		return ($exhibition) ? $exhibition : false;
	}

	public function UpcomingExhibitions() {
		$today = date('Y-m-d');

		$exhibitions = Exhibit::get()->filter(array("StartDate:GreaterThan" => $today))->sort("StartDate", "ASC");

		return $exhibitions->count() ? $exhibitions : false;
	}

    public function getCurrentSlides() {

        $today = date('Y-m-d G:i:s');

//        $slides = $this->owner->Slides()->filter(
//            array(
//                "PublishDate:LessThanOrEqual" => $today,
//                "UnpublishDate:GreaterThanOrEqual" => $today
//            )
//        );

        $slides = $this->owner->Slides();

        $activeSlides = new ArrayList();

        foreach ($slides as $slide) {

            if(!$slide->PublishDate && !$slide->UnpublishDate) { // if No dates then Push
                $activeSlides->push($slide);
            } elseif ($slide->PublishDate && $slide->PublishDate <= $today) {
                if (!$slide->UnpublishDate || $slide->UnpublishDate >= $today ) { // if start date in past and end date is future then push
                    $activeSlides->push($slide);
                }
            } elseif (!$slide->PublishDate && $slide->UnpublishDate) {
                if($slide->UnpublishDate >= $today ) { //id no start date and end date in future
                    $activeSlides->push($slide);
                }
            } elseif ($slide->PublishDate && !$slide->UnpublishDate) {
                if($slide->PublishDate <= $today) { // if start date in future and no end date
                    $activeSlides->push($slide);
                }
            }
        }


        return ($activeSlides->count()) ? $activeSlides : false;

    }

}


class HomePage_Images extends DataObject {

    static $db = array (
        'PageID' => 'Int',
        'HomePageID' => 'Int',
        'ImageID' => 'Int',
        'Caption' => 'Text',
        'SortOrder' => 'Int'
    );


}

class OenoArtwork extends DataExtension {

	private static $db = array(
		"NoPrice" => "Boolean",
		"Outdoor" => "Boolean",
		"Secondary" => "Boolean",
        "ShowOnWeb" => "Boolean",
        "IsSold" => "Boolean",
		"Sort" => "Int",
        "Featured" => "Boolean"

	);

	private static $has_one = array(

	);

	private static $has_many = array(

	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab("Root.MasterPiece", ReadonlyField::create("NoPrice")->setDescription('Value Copied from Artist'));

		$fields->removeFieldsFromTab("Root.Main", array(
			"Outdoor",
			"Secondary",
	        "ShowOnWeb",
	        "IsSold",
			"Featured",

		));

		$artworkOptions = ToggleCompositeField::create(
			"ArtworkOptions",
			"Options",
			array(
				//CheckboxField::create("NoPrice", "Hide Price"),
				CheckboxField::create("Outdoor"),
				CheckboxField::create("Secondary", "Secondary Market"),

				CheckboxField::create("IsSold"),
				CheckboxField::create("Featured")
			)
		);

		$fields->addFieldToTab("Root.Main", $artworkOptions);
		$fields->addFieldToTab("Root.Main", HiddenField::create("Sort"));
		$fields->addFieldToTab("Root.Main", HiddenField::create("LocationsID"));
		$fields->insertAfter(CheckboxField::create("ShowOnWeb"), 'Year');
	}

	public function SearchArt() {
		return Controller::curr()->SearchArt();
	}
}

class OenoExhibit extends DataExtension {

	private static $db = array(
		"Summary" => "HTMLText"
	);

	private static $has_one = array(

	);

	private static $has_many = array(

	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab("Root.Main", HTMLEditorField::create("Summary")->setRows(4)->setDescription('Short Description for HomePage list'), "Content");
	}

}


class Exhibit_Images extends DataObject {

    static $db = array (
        'PageID' => 'Int',
        'ExhibitID' => 'Int',
        'ImageID' => 'Int',
        'Caption' => 'Text',
        'SortOrder' => 'Int'
    );


}

class OenoSiteConfig extends DataExtension {

	private static $db = array(

	);

	private static $has_one = array(
		"DefaultThankYou" => "Page"
	);

	private static $has_many = array(
		"TourismLinks" => "SiteLink"
	);

	public function updateCMSFields(FieldList $fields) {

		$linkField = new GridField("LinkList", "Tourism Links", $this->owner->TourismLinks());

		$config = GridFieldConfig_RelationEditor::create();

		$linkField->setConfig($config);

		$fields->addFieldToTab("Root.Link", $linkField);
		$fields->addFieldsToTab("Root.Main", TreeDropdownField::create('DefaultThankYouID', 'Default Thank You Page', 'SiteTree'));

	}
}


class OenoMember extends DataExtension {

    private static $db = array(
        "JobTitle" => "Varchar(255)",
        "ShowAsContact" => "Boolean",
        "PublicEmail" => "Varchar",
        "SortOrder" => "Int"

    );

    private static $many_many = array(
    	"VaultArtworks" => "VaultArtwork"
    );

    public function updateCMSFields(FieldList $fields) {

    	$fields->removeByName('VaultArtworks');

        $fields->insertAfter(Textfield::create("JobTitle"), "Email");
        $fields->insertAfter(CheckboxField::create("ShowAsContact", "Show on Contact Us Page"), "JobTitle");
        $fields->insertAfter(EmailField::create("PublicEmail"), "ShowAsContact");
        $fields->insertAfter(NumericField::create("SortOrder", "Sort Order"), "PublicEmail");

	    if($this->owner->ID != 0) {

		    $vaultArtworkFieldConfig = GridFieldConfig_RelationEditor::create();
		    $vaultArtworkFieldConfig->addComponent(new GridFieldOrderableRows('Sort'));
		    $vaultArtworkFieldConfig->removeComponentsByType('GridFieldAddNewButton');

		    $vaultArtworkField = GridField::create("VaultArtworks", "Vault Artworks",$this->owner->VaultArtworks());
		    $vaultArtworkField->setConfig($vaultArtworkFieldConfig);

		    $fields->addFieldsToTab("Root.Vault", $vaultArtworkField);

		    $vaultReadOnly = GridField::create( "VaultArtwork", "All Vault", VaultArtwork::get() );
//
		    $vaultReadOnlyConfig = $vaultArtworkField->getConfig();
			$vaultReadOnlyConfig->removeComponentsByType('GridFieldOrderableRows');

		    $sortField = $vaultReadOnly->getConfig()->getComponentByType('GridFieldSortableHeader');

		    $sortField->setFieldSorting(array("ArtistName" => "ArtistName"));

//		    $vaultReadOnlyConfig->removeComponentsByType('GridFieldSortableHeader');
//
//		    $sortableHeader = new GridFieldSortableHeader();
//			$sortableHeader->setFieldSorting(array('ArtistName'=> "ArtistName"));
//
//		    $vaultReadOnlyConfig->addComponent( $sortableHeader );
////		    $vaultReadOnlyConfig->getComponentByType('GridFieldSortableHeader')->setFieldSorting(array(
////		    	"Title" => "Title",
////			    "ArtistName" => "ArtistName"
////		    ));
//
//		    $vaultReadOnly->setConfig($vaultReadOnlyConfig);


		    $fields->addFieldToTab("Root.Vault", $vaultReadOnly);

	    }


    }

}

class OenoSecurityAdmin extends DataExtension {
	public function updateEditForm(FieldList $form) {

		$memberField = $form->Fields()[0]->getChildren()->fieldByName('Users')->fieldByName('Members');

		$memberFieldConfig = $memberField->getConfig();

		$memberFieldConfig->addComponent(new GridFieldSortableRows('SortOrder'));

	}



}

class OenoContactUs extends DataExtension {
    public function StaffContacts() {
        $members = Member::get()->filter(array("ShowAsContact" => 1));

        return $members->count() ? $members->sort('SortOrder') : false;
    }
}

class OenoContactController extends DataExtension {

    public static $allowed_actions = array(
        'ContactForm',
    );

    // Template method
    public function ContactForm() {


        $fields = FieldList::create(
            TextField::create('Name', false)
                     ->setAttribute('placeholder', 'First and Last Name')
                     ->addExtraClass('has-placeholder')
                     ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                      ->setAttribute('placeholder', 'Email (required)')
                      ->addExtraClass('has-placeholder half-field left-field')
                      ->setRightTitle("Email")
                      ->setAttribute('required', true),
            TextField::create('Phone', false)
                     ->setAttribute('placeholder', 'Phone Number')
                     ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                     ->setAttribute('title', 'A Valid International or North American Phone Number')
                     ->addExtraClass('has-placeholder half-field')
                     ->setRightTitle("Phone Number"),
            TextareaField::create('Message')
                         ->addExtraClass('has-placeholder'),
            CheckboxField::create('Subscribe', 'Please sign me up for the Oeno Gallery Newsletter')
        );

        $required = new RequiredFields(array(
            'Email', 'Message'
        ));

        $form = Form::create(
            $this->owner,
            "ContactForm",
            $fields,
            new FieldList(
                FormAction::create('doContact', 'Submit')->addExtraClass("button primary")//->setDisabled(true)
            ),
            $required
        );

        return $form->enableSpamProtection(array(
            'mapping' => array(
                'Name' => 'authorName',
                'Email' => 'authorMail',
                'Message' => 'body'
            )
        ));
    }

    public function doContact($data, Form $form) {

        //Manual Block


        $galleryEmail = new Email();

        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');

        $galleryEmail->setFrom($galleryFrom)
                     ->setTo($galleryTo)
                     ->replyTo($data['Email'])
                     ->setSubject('Contact From '.$data['Name'])
                     ->setBody($data['Message']);


        $galleryEmail->send();

        $form->addErrorMessage('Message', 'Thank you, someone from our office will contact you shortly', 'success');
        return Controller::curr()->redirectBack();

    }
}
