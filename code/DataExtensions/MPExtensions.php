<?php

class MPArtwork extends DataExtension {

	private static $db = array(
		"MPTitleID" => "Int",
		"MPArtistID" => "Int",
		"MPScanCode" => "Varchar",
		"MPArtCode" => "Varchar",
		"MPTitleText1" => "Text",
		"MPTitleText2" => "Text",
		"MPTitleText3" => "Text",
		"MPCustom1" => "Text",
		"MPCustom2" => "Text",
		"MPCustom3" => "Text",
		"MPDateAdded" => "SS_Datetime",
		"MPDateUpdated" => "SS_Datetime",
		"MPImages" => "Int",
		"Historical" => "Boolean",
		"IsLatest" => "Boolean",
		"MPDescription" => "Text"
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab("Root.MasterPiece", array(

			ReadonlyField::create(	"MPTitleID"	  	),
			ReadonlyField::create(	"MPArtistID"  	),
			ReadonlyField::create(	"MPScanCode"  	),
			ReadonlyField::create(	"MPArtCode"	  	),
			ReadonlyField::create(	"MPTitleText1"	),
			ReadonlyField::create(	"MPTitleText2"	),
			ReadonlyField::create(	"MPTitleText3"	),
			ReadonlyField::create(	"MPCustom1"	  	),
			ReadonlyField::create(	"MPCustom2"	  	),
			ReadonlyField::create(	"MPCustom3"	  	),
			ReadonlyField::create(	"MPDateAdded" 	),
			ReadonlyField::create(	"MPDateUpdated"	),
			ReadonlyField::create(	"MPImages"		),
			ReadonlyField::create(	"MPDescription"	),
			CheckboxField::create(	"Historical"	),

		));

	}
}


class MPArtist extends DataExtension {

	private static $db = array(
		"MPArtistID" => "Int",
		"MPDateAdded" => "SS_Datetime",
		"MPDateUpdated" => "SS_Datetime",
		"MPHasImage" => "Boolean",
		"MPHasBio" => "Boolean",
		"MPDescription" => "Text",
		"MPFirstTitle" => "Int"
	);


	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab("Root.MasterPiece", array(
			ReadonlyField::create(	"MPArtistID"   	),
			ReadonlyField::create(	"MPDateAdded"  	),
			ReadonlyField::create(	"MPDateUpdated"	),
			ReadonlyField::create(	"MPHasImage"   	),
			CheckboxField::create(	"MPHasBio"	   	),
			TextField::create(      "MPDescription"	),
			ReadonlyField::create(	"MPFirstTitle"	),
		));



	}

	public function getBioLink() {
		$companyId = "1A95-CCGH-6E59";
		return "https://masterpiecesolutions.org/common/htmlbio.php?galleryId=".$companyId."&artistId=".$this->owner->MPArtistID;
	}

	public function getMainArtwork() {
		if($this->owner->MPFirstTitle != 0) {

			$artwork = Artwork::get()->filter(array("MPTitleId" => $this->owner->MPFirstTitle))->First();

			if(!$artwork || $artwork->Quantity < 1) {
				$art = $this->owner->FirstThumb();
			} else {
				$art = $artwork->OrderedImages()->First();
			}

			return ($art) ? $art : false;

		} elseif ($this->owner->FirstThumb()) {

			return $this->owner->FirstThumb();

		} else {
			return false;
		}
	}

	public function IsHistorical() {
		return $this->owner->getMainArtwork()->Historical ? true : false;
	}


}

class MPExhibit extends DataExtension {

	private static $db = array(
		"MPDateAdded" => "SS_Datetime",
		"MPDateUpdated" => "SS_Datetime",
		"MPEventID" => "Int",
		"MPDescription" => "Text",
		"MPLocation" => "Varchar(255)",
		"MPFeatured" => "Varchar(255)",
		"MPEventTite" => "Varchar(255)"
	);



	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab("Root.MasterPiece", array(
			ReadonlyField::create(	"MPDateAdded"	),
			ReadonlyField::create(	"MPDateUpdated"	),
			ReadonlyField::create(	"MPEventID"	),
			ReadonlyField::create(	"MPDescription"	),
			ReadonlyField::create(	"MPLocation"	),
			ReadonlyField::create(	"MPFeatured"	),
			ReadonlyField::create(	"MPEventTite"	)
		));
	}


}
