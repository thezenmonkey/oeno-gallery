<?php
	
class RangeField extends FormField {
	/**
	 * @var int
	 */
	protected $maxValue;
	protected $minValue;
	
	/**
	 * Returns an input field, class="text" and type="text" with an optional maxlength
	 */
	public function __construct($name, $title = null, $value = '', $maxValue = 10, $minValue = 1, $form = null) {
		$this->maxValue = $maxValue;
		$this->minValue = $minValue;
		
		parent::__construct($name, $title, $value, $form);
	}
	
	
	public function Field($properties = array()) {
		
		return CompositeField::create(
			array(
				NumericField::create($this->Name.'_min', 'Min', $this->getMinValue()),
				NumericField::create($this->Name.'_max', 'Max', $this->getMaxValue())
			)
		);
	}
	
	/**
	 * @param int $value
	 */
	public function setMaxValue($value) {
		$this->maxValue = $value;
		
		return $this;
	}
	
	/**
	 * @param int $value
	 */
	public function setMinValue($value) {
		$this->imnValue = $value;
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getMaxValue() {
		return $this->maxValue;
	}
	
	/**
	 * @return int
	 */
	public function getMinValue() {
		return $this->minValue;
	}

	public function getAttributes() {
		return array_merge(
			parent::getAttributes(),
			array(
				'min' => $this->getMinValue(),
				'max' => $this->getMaxValue()
			)
		);
	}

	public function InternallyLabelledField() {
		if(!$this->value) $this->value = $this->Title();
		return $this->Field();
	}
}